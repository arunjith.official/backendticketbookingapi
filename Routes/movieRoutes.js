var express     = require('express');
const router = express.Router()


const { getMovies, createMovie, getMovieByID } = require('../controller/MovieController');



router.get("/", async (req, res) => {
    const movie = await getMovies()
    console.log(movie)
    res.json({
        movie: movie
    })
})


router.get("/:movie_id", async (req, res) => {
    const movie = await getMovieByID(req.params.movie_id)
    res.json(movie)
})

router.post("/", async (req, res) => {
    const movie_name = req.body.movie_name
    const movie_duration = req.body.movie_duration
    const release_date = req.body.release_date
    const movie_description = req.body.movie_description

    console.log(req.body)

    createMovie(movie_name, movie_duration, release_date, movie_description)
        .then((result) => {
            console.log(result)
            res.status(201).send("succcess")
        }
        )
})


module.exports = router