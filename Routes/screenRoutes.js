var express     = require('express');
const router = express.Router()




const { getScreen, createNewScreen } = require('../controller/ScreenController')


router.get("/", async (req, res) => {
    const screen = await getScreen()
    console.log(screen)
    res.json({
        screen: screen
    })

})


router.post('/', async (req, res) => {
    const screen_id = req.body.screen_id
    const total_seats = req.body.total_seats
    const screen_name = req.body.screen_name
    const screen_description = req.body.screen_description
    createNewScreen(screen_id, total_seats, screen_name, screen_description)
        .then((result) => {
            console.log(result)
            res.status(201).send("success")
        }
        )
})



module.exports = router