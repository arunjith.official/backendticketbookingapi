var express     = require('express');
const router = express.Router()




const { createSeatsBooked, getSeatsBooked, getSeatsBookedByShowID, getSeats, getSeatsBookedByBookingID } = require('../controller/Seats-BookedController')


router.post("/", async (req, res) => {
    const seat_id = req.body.seat_id
    const booking_id = req.body.booking_id
    const show_id = req.body.show_id
    createSeatsBooked(seat_id, booking_id, show_id)
        .then((result) => {
            console.log(result)
            res.status(201).send("success")
        })
        .catch((error) => {
            console.error(error)
        })
})

router.get("/", async (req, res) => {
    const seats_booked = await getSeatsBooked()
    console.log(seats_booked)
    res.json({
        seats_booked: seats_booked
    })
})

router.get("/:show_id", async (req, res) => {
    const seatsBooked = await getSeatsBookedByShowID(req.params.show_id)
    res.json(seatsBooked)
})

router.get("/seats-booked/booking/:booking_id", async (req, res) => {
    const seatsBooked = await getSeatsBookedByBookingID(req.params.booking_id)
    res.json(seatsBooked)
})

module.exports = router