var express     = require('express');
const router = express.Router()




var bcrypt = require('bcrypt');
const saltRounds = 10;


const { getUsersAndPassword, createNewUser, checkUser } = require('../controller/usersController.js')


router.get("/", async (req, res) => {
    const users = await getUsersAndPassword()
    console.log(users)
    res.json({
        users: users
    })
})

router.post("/", async (req, res) => {
    const username = req.body.username
    const password = req.body.password
    var hashedPassword
    bcrypt.genSalt(saltRounds, function (err, salt) {
        bcrypt.hash(password, salt, function (err, hash) {
            hashedPassword = hash
            createNewUser(username, hashedPassword)
                .then((result) => {
                    res.status(201).send("success")
                })
                .catch((error) => {
                    console.error(error)
                })
        })
    })
})

router.get("/user-check", async (req, res) => {
    const username = req.body.username
    const password = req.body.password
    const status = await checkUser(username, password)
    console.log(status)
    return status
})

module.exports = router