var express     = require('express');
const router = express.Router()



const { createBooking, getBooking, getBookingsByPhone, getbookingsByBookingID } = require('../controller/BookingController')

router.post("/book", async (req, res) => {
    const booked_by = req.body.booked_by
    const phone = req.body.phone
    const show_id = req.body.show_id
    console.log(`received v${req.body.booked_by}`)
    const booking = await createBooking(booked_by, phone, show_id)
    res.status(200).json(booking)
    // .then((result => {
    //   console.log(result)
    //   res.status(201).send("success")
    //   return result
    //   }))
})


router.get("/", async (req, res) => {
    const bookings = await getBooking()
    console.log(bookings)
    res.json({
        bookings: bookings
    })
})


router.get("/:booking_id", async (req, res) => {
    const bookings = await getbookingsByBookingID(req.params.booking_id)
    res.json(bookings)
})

router.get("/phone/:phone", async (req, res) => {
    const bookings = await getBookingsByPhone(req.params.phone)
    res.json(bookings)
})

module.exports = router