var express     = require('express');
const router = express.Router()



const { getScreening, createNewScreening, getScreeningByMovieID } = require('../controller/ScreeningController')


router.get("/", async (req, res) => {
    const screening = await getScreening()
    console.log(screening);
    res.json({
        screening: screening
    })
})

router.get("/:movie_id", async (req, res) => {
    const screening = await getScreeningByMovieID(req.params.movie_id)
    res.json(screening)
})


router.post("/", async (req, res) => {
    const movie_id = req.body.movie_id
    const screen_id = req.body.screen_id
    const show_time = req.body.show_time

    createNewScreening(movie_id, screen_id, show_time)
        .then((result) => {
            console.log(result)
            res.status(201).send("succcess")
        }
        )

})

module.exports = router