const { Sequelize, DataTypes, Model } = require('sequelize');

const sequelize = require("./connection")


// TO ADD SCREENING
const Screening = require("./schemas/ScreeningSchema")


//SCREEN
const Screen = require("./schemas/ScreenSchema")


// BOOKING 

const Booking = require("./schemas/BookingSchema")

//MOVIE

const Movie = require("./schemas/MovieSchema")

// SEATS_BOOKED


const SeatsBooked = require("./schemas/Seats-bookedSchema")


