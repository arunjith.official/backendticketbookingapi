const { Sequelize, DataTypes, Model } = require('sequelize');

const sequelize = new Sequelize('cinema_db', 'root', 'Chippy@s3d', {
    host: 'localhost',
    dialect: "mysql"/* one of 'mysql' | 'postgres' | 'sqlite' | 'mariadb' | 'mssql' | 'db2' | 'snowflake' | 'oracle' */
  });

  (async () => {
    try {
      await sequelize.authenticate();
      console.log('Connection has been established successfully.');
    } catch (error) {
      console.error('Unable to connect to the database:', error);
    }
  
  })();

module.exports = sequelize