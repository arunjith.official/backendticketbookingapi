class Screen {
    constructor(){
        this.seats=[
            {booked:false},
            {booked:false},
            {booked:false},
            {booked:false},
            {booked:false},
            {booked:false},
            {booked:false},
            {booked:false},
            {booked:false},
            {booked:false}
        ]
    }
    getSeats () {
        return this.seats
    }
    bookSeat (seatnumber){
        if (this.seats[seatnumber]==false){
            this.seats[seatnumber].booked=true
            return true
        }
        else {
            return false
        }
    }
    cancelSeat (seatnumber){
        this.seats[seatnumber].booked=true
    }
}
module.exports=Screen