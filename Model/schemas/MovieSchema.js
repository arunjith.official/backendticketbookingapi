const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = require('../connection');

class Movie extends Model {}
Movie.init({
    // Model attributes are defined here
    movie_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement:true,
      primaryKey:true
    },
    movie_name: {
      type: DataTypes.STRING
    },
    movie_duration: {
        type: DataTypes.FLOAT
    },
    release_date: {
        type: DataTypes.DATEONLY,
        defaultValue: DataTypes.NOW
    },
    movie_description: {
      type: DataTypes.STRING
    }
  }, 
  {
    // Other model options go here
    tableName:"movie",
    timestamps: false,
    sequelize, // We need to pass the connection instance
    modelName: 'Movie' // We need to choose the model name
  });
  (async () => {
    await sequelize.sync();
    console.log("All models were synchronized successfully.");
  })();

  module.exports = Movie