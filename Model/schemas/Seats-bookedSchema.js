const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = require('../connection');

class SeatsBooked extends Model {}

SeatsBooked.init({
  // Model attributes are defined here
  booking_id: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  seat_id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    primaryKey:true
  },
  show_id: {
    type:  DataTypes.INTEGER,
    allowNull: false,
    primaryKey:true
  }
}, {
  // Other model options go here
  tableName:"seats_booked",
  timestamps: false,
  sequelize, // We need to pass the connection instance
  modelName: 'SeatsBooked' // We need to choose the model name
});
(async () => {
  await sequelize.sync();
  console.log("All models were synchronized successfully.");
})();

module.exports = SeatsBooked