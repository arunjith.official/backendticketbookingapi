const express = require('express')
const moment = require('moment')
var cors = require('cors')




const userRouter = require("./Routes/userRoutes")
const seatsRouter = require("./Routes/seats-bookedRoutes")
const screenRouter = require("./Routes/screenRoutes")
const screeningRouter = require("./Routes/screeningRoutes")
const movieRouter = require("./Routes/movieRoutes")
const bookingRouter = require("./Routes/bookingRoutes")


const app = express()
const port = 3000

app.use(express.json())
app.use(cors())


app.use("/user", userRouter)
app.use("/seats-booked", seatsRouter)
app.use("/screen", screenRouter)
app.use("/screening", screeningRouter)
app.use("/movie",movieRouter)
app.use("/book", bookingRouter)
app.get('/', (req, res) => {
  res.send('Hello World!')
})




app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})