const Users = require("../Model/schemas/UsersSchema.js")
const sequelize = require('../Model/connection');
var bcrypt = require('bcrypt');
const saltRounds = 10;

const getUsersAndPassword = async () => {
    const users = await Users.findAll({
        attributes: ['username', 'password']
    })
    console.log(users)
    const userArray = users.map(users => users.dataValues)
    return userArray
}

const createNewUser = async (username, password) => {
    const users = await Users.create({
        username,
        password
    })
    console.log(users)
}

const checkUser = async (username, password) => {
    console.log(username)
    const result = await sequelize.query(
        `
        SELECT * FROM users
        WHERE users.username= "${username}";`, { type: sequelize.QueryTypes.SELECT }
    )
    console.log(result)
    const salt = await bcrypt.genSalt(saltRounds)
    const hashedPassword = await bcrypt.hash(password,salt)
    console.log(hashedPassword)
    function checkPassword(result, hashedPassword) {
        if (result.password == hashedPassword) {
            return true
        }
        else if (result.password != hashedPassword) {
            return false
        }
    }
    const status = checkPassword(result, hashedPassword)
    return status
}

exports.getUsersAndPassword = getUsersAndPassword;
exports.createNewUser = createNewUser;
exports.checkUser = checkUser;