const Movie = require("../Model/schemas/MovieSchema")

const createMovie = async (movie_name, movie_duration, release_date, movie_description) => {
    const movie = await Movie.create({
        movie_name,
        movie_duration,
        release_date,
        movie_description
    })
}

const getMovies = async () => {
    const movie = await Movie.findAll({
        attributes: ['movie_id', 'movie_name', 'movie_duration', 'release_date', 'movie_description']
    })
    const moviesArray = movie.map(movie => movie.dataValues)
    return moviesArray
}

const getMovieByID = async (movie_id) => {
    const movie = await Movie.findByPk(movie_id)
    return movie
}

exports.createMovie = createMovie;
exports.getMovieByID = getMovieByID;
exports.getMovies = getMovies;
