const { Sequelize, DataTypes, Model } = require('sequelize');
const Screening = require("../Model/schemas/ScreeningSchema")


const getScreening = async () => {
    const screening = await Screening.findAll({
        attributes: ['show_id', 'show_time', 'movie_id', 'screen_id']
    });
    const screeningArray = screening.map(screening => screening.dataValues)
    return screeningArray
}

const createNewScreening = async (movie_id,screen_id,show_time) => {
    const newscreening = await Screening.create({
        movie_id, 
        screen_id,
        show_time
    })
}

const getScreeningByMovieID = async (movie_id) => {
    const screening = await Screening.findAll({
        where: { movie_id: movie_id }
    })
    return screening
}


exports.getScreeningByMovieID = getScreeningByMovieID;
exports.getScreening = getScreening;
exports.createNewScreening = createNewScreening;
